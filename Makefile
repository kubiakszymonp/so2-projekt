.DEFAULT_GOAL := run
compile_run:
	g++ -o output main.cpp Ball/*.cpp Position/*.cpp Board/*.cpp  BallEater/*.cpp  -lncurses -pthread

run: compile_run
	./output