#pragma once
#include "../Position/Position.hpp"

class Object
{
public:
    virtual void print() {}
    virtual Position getPosition()
    {
        Position p;
        return p;
    }
    virtual void run() {}
};