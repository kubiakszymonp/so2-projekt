#include "Board.hpp"
#include "../Ball/Ball.hpp"
#include "../Position/Position.hpp"
#include "../Board/Board.hpp"
#include <unistd.h>
#include <ncurses.h>
#include <iostream>

void Board::refreshOnce()
{
    erase();
    mvprintw(0, 0, std::to_string(this->objects.size()).c_str());
    for (Object *b : this->objects)
    {
        b -> print();
    }
    refresh();
}

void Board::refreshingLoop()
{
    int i = 0;
    while (true)
    {
        this->refreshOnce();

        usleep(1000 * 100);
    }
}