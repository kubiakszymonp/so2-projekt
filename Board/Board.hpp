#pragma once
#include "../Object/Object.hpp"
#include <vector>

class Board
{
public:
    int borderY;
    int borderX;
    std::vector<Object *> objects;

    Board(int borderY, int borderX)
    {
        this->borderX = borderX;
        this->borderY = borderY;
    }

    void refreshingLoop();

    void refreshOnce();
};