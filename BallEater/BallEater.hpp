#pragma once
#include "../Position/Position.hpp"
#include "../Object/Object.hpp"
#include <ncurses.h>
#include "../Board/Board.hpp"

class BallEater : public Object
{
public:
    Position position;
    int height;
    int width;
    Board *board;
    const char *display = "@";

    BallEater(int posX, int posY, int vecX, int vecY, int borderX, int borderY, int width, int height, Board *board)
        : position(posX, posY, vecX, vecY, borderX, borderY, width, height)
    {
        this->board = board;
        this->width = width;
        this->height = height;
    }

    virtual Position getPosition()
    {
        return this->position;
    }

    virtual void print()
    {

        Position p = this->getPosition();

        int startX = p.posX - this->width;
        int startY = p.posY - this->height;

        for (int i = 0; i < this->height; i++)
        {
            for (int j = 0; j < this->width; j++)
            {

                mvprintw(startY + i, startX + j, this->display);
            }
        }
    }

    void makeMove()
    {
        this->position.makeMove();
    }

    void eatBalls();

    void run();

    bool isOnMyField(Object * obj);
};