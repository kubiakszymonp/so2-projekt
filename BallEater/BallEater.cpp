#include "BallEater.hpp"
#include <unistd.h>
#include <algorithm>
#include "../Ball/Ball.hpp"

void BallEater::run()
{
    while (true)
    {
        this->makeMove();
        this->eatBalls();
        usleep(1000 * 100);
    }
}

bool BallEater::isOnMyField(Object *obj)
{
    Position objPos = obj->getPosition();
    Position thisPos = this->getPosition();

    bool matchesX = objPos.posX <= thisPos.posX + thisPos.width && objPos.posX >= thisPos.posX - thisPos.width;
    bool matchesY = objPos.posY <= thisPos.posY + thisPos.height && objPos.posY >= thisPos.posY - thisPos.height;

    if (matchesX && matchesY && obj != this)
    {
        Ball *b = (Ball *)obj;
        b->isEaten = true;
        return true;
    }
    return false;
}

void BallEater::eatBalls()
{
    auto iteratorBegin = this->board->objects.begin();
    auto iteratorEnd = this->board->objects.end();
    auto new_end = std::remove_if(iteratorBegin, iteratorEnd, [this](Object *obj)
                                  { return this->isOnMyField(obj); });
    this->board->objects.erase(new_end, iteratorEnd);
}