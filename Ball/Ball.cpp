#include "Ball.hpp"
#include <unistd.h>
#include <iostream>
#include <algorithm>

void Ball::run()
{
    while (true)
    {
        if(this-> isEaten){
            break;
        }
        if (this->wallHitCounter >= 6)
        {
            auto iteratorBegin = this->board->objects.begin();
            auto iteratorEnd = this->board->objects.end();
            auto new_end = std::remove_if(iteratorBegin, iteratorEnd, [this](Object *obj)
                                          { return this == obj; });
            this->board->objects.erase(new_end, iteratorEnd);
            break;
        }

        usleep(1000 * 100);

        bool hitWall = this->makeMove();
        if (hitWall)
        {
            this->wallHitCounter++;
        }
    }
}