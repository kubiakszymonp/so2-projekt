#pragma once
#include "../Position/Position.hpp"
#include "../Board/Board.hpp"
#include "../Object/Object.hpp"
#include <ncurses.h>
#include <iostream>


class Ball : public Object
{
public:

    Board * board;
    Position position;
    int wallHitCounter;
    bool isEaten;

    const char *display = "b";



    Ball(int posX, int posY, int vecX, int vecY, int borderX, int borderY, Board *board)
        : position(posX, posY, vecX, vecY, borderX, borderY)
    {
        this->board = board;
        this->wallHitCounter = 0;
        this->isEaten = false;
        // std::cout<< posX<<std::endl;
        // std::cout<< posY<<std::endl;
        // std::cout<< vecX<<std::endl;
        // std::cout<< vecY<<std::endl;
        // std::cout<< borderX<<std::endl;
        // std::cout<< borderY<<std::endl;
        // std::cout<< board->objects.size()<<std::endl;
    }

    bool makeMove()
    {
        return this->position.makeMove();
    }

    virtual Position getPosition() { return this->position; }

    virtual void run();
    virtual void print()
    {
        Position p = this->getPosition();
        // mvprintw(1,1, std::to_string(p.posY).c_str());
        // mvprintw(2,1, std::to_string(p.posX).c_str());
        // mvprintw(3,1, std::to_string(this->wallHitCounter).c_str());
        mvprintw(p.posY, p.posX, this->display);
    }
};