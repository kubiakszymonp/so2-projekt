#include <iostream>
#include <thread>
#include <unistd.h>
#include <ncurses.h>
#include "Ball/Ball.hpp"
#include "Board/Board.hpp"
#include "Object/Object.hpp"
#include "BallEater/BallEater.hpp"
#include "BallCreator/BallCreator.hpp"

int main()
{
    initscr();
    curs_set(0);
    int x = 10, y = 10;
    getmaxyx(stdscr, y, x);

    Board *board = new Board(x, y);
    std::thread boardDisplayThread([board]
                                   { board->refreshingLoop(); });

    Object *ballEater = new BallEater(10, 10, 1, 0, x, y, 10, 5, board);
    board->objects.push_back(ballEater);
    std::thread ballEaterThread([ballEater]
                                { ballEater->run(); });


    BallCreator *ballCreator = new BallCreator(board);
    std::thread ballCreatorThread([ballCreator]
                                  { ballCreator->run(); });

    ballEaterThread.join();
    boardDisplayThread.join();

    ballCreatorThread.join();

    endwin();
    return 0;
}