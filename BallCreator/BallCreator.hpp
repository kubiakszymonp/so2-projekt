#include "../Board/Board.hpp"
#include "../Ball/Ball.hpp"
#include "../Object/Object.hpp"
#include <ncurses.h>
#include <unistd.h>
#include <random>

class BallCreator
{
public:
    Board *board;

    std::random_device dev;
    std::mt19937 rng;
    std::uniform_int_distribution<std::mt19937::result_type> ballGenInterval;
    std::uniform_int_distribution<std::mt19937::result_type> xVec;
    std::uniform_int_distribution<std::mt19937::result_type> yVec;

    BallCreator(Board *board) : rng(this->dev()),
                                ballGenInterval(2000, 6000),
                                xVec(1, 3),
                                yVec(1, 3)
    {
        this->board = board;
    }

    void run()
    {
        while (true)
        {
            int x = this->board->borderX;
            int y = this->board->borderY;

            Object *ball = new Ball(10, x - 1, -xVec(this->rng), -yVec(this->rng), y, x, this->board);
            this->board->objects.push_back(ball);
            std::thread ballThread([ball]
                                   { ball->run(); });
            ballThread.detach();
            usleep(1000 * ballGenInterval(this->rng));
            refresh();
        }
    }
};