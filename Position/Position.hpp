#pragma once
#include <string>

class Position
{
public:
    int vecX;
    int vecY;
    int posX;
    int posY;
    int borderX;
    int borderY;
    int height;
    int width;

    Position(int posX, int posY, int vecX, int vecY, int borderX, int borderY, int width, int height)
    {
        this->posX = posX;
        this->posY = posY;
        this->vecX = vecX;
        this->vecY = vecY;
        this->borderX = borderX;
        this->borderY = borderY;
        this->width = width;
        this->height = height;
    }
    
    Position() {}

    Position(int posX, int posY, int vecX, int vecY, int borderX, int borderY)
    {
        this->posX = posX;
        this->posY = posY;
        this->vecX = vecX;
        this->vecY = vecY;
        this->borderX = borderX;
        this->borderY = borderY;
        this->width = 0;
        this->height = 0;
    }

    bool makeMove();

    std::string getPositionLog()
    {
        std::string str = "posX: " + std::to_string(this->posX);
        str += "\n";
        str += "posY: " + std::to_string(this->posY);
        str += "\n";
        str += "vecX: " + std::to_string(this->vecX);
        str += "\n";
        str += "vecY: " + std::to_string(this->vecY);
        str += "\n";
        return str;
    }
};