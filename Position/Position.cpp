#include "Position.hpp"

bool Position::makeMove()
{
    bool hitWall = false;
    // upper and lower bound
    if (this->posY + this->vecY - this->height < 0 ||
        this->posY + this->vecY + this->height >= this->borderY)
    {
        hitWall = true;
        this->vecY = -this->vecY;
    }

    // left and right bound
    if (this->posX + this->vecX - this->width < 0 ||
        this->posX + this->vecX + this->width >= this->borderX)
    {
        hitWall = true;
        this->vecX = -this->vecX;
    }

    this->posX += this->vecX;
    this->posY += this->vecY;

    return hitWall;
}